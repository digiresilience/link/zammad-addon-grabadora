Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match api_path + "/channels_cdr_grabadora", to: "channels_cdr_grabadora#index", via: :get
  match api_path + "/channels_cdr_grabadora", to: "channels_cdr_grabadora#add", via: :post
  match api_path + "/channels_cdr_grabadora/:id", to: "channels_cdr_grabadora#update", via: :put
  match api_path + "/channels_cdr_grabadora_webhook/:token", to: "channels_cdr_grabadora#webhook", via: :post
  match api_path + "/channels_cdr_grabadora_disable", to: "channels_cdr_grabadora#disable", via: :post
  match api_path + "/channels_cdr_grabadora_enable", to: "channels_cdr_grabadora#enable", via: :post
  match api_path + "/channels_cdr_grabadora", to: "channels_cdr_grabadora#destroy", via: :delete
  match api_path + "/channels_cdr_grabadora_rotate_token", to: "channels_cdr_grabadora#rotate_token", via: :post
end
