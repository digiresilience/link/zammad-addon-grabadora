class Controllers::ChannelsCdrGrabadoraControllerPolicy < Controllers::ApplicationControllerPolicy
  default_permit!("admin.channel_grabadora")
end
