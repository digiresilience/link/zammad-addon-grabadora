class GrabadoraChannel < ActiveRecord::Migration[5.2]
 def self.up
    Ticket::Article::Type.create_if_not_exists(
      name: "cdr_grabadora",
      communication: false,
      updated_by_id: 1,
      created_by_id: 1,
    )
    Permission.create_if_not_exists(
      name: "admin.channel_cdr_grabadora",
      note: "Manage %s",
      preferences: {
        translations: ["Channel - Grabadora"],
      },
    )
  end

  def self.down
    t = Ticket::Article::Type.find_by(name: "cdr_grabadora")

    if t
      t.destroy
    end

    p = Permission.find_by(name: "admin.channel_cdr_grabadora")
    if p
      p.destroy
    end
  end
end
