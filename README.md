# zammad-addon-grabadora

An addon that adds [grabadora](https://gitlab.com/digiresilience/link/grabadora) channel to Zammad.


## Grabadora Channel

This channel creates a new channel "Grabadora".


To submit a ticket: make a POST to the Submission Endpoint with the header
`Authorization: SUBMISSION_TOKEN`.

The payload must be a json object with the keys:

* `startTime` - string containing ISO date
* `endTime` - string containing ISO date
* `to` - fully qualified phone number
* `from` - fully qualified phone number
* `duration` - string containing the recording duration
* `callSid` - the unique identifier for the call
* `recording` - string base64 encoded binary of the recording
* `mimeType` - string of the binary mime-type


## Development

1. Edit the files in `src/`

   Migration files should go in `src/db/addon/grabadora` ([see this post](https://community.zammad.org/t/automating-creation-of-custom-object-attributes/3831/2?u=abelxluck))

2. Update version and changelog in `grabadora-skeleton.szpm`
3. Build a new package `make`

   This outputs `dist/grabadora-vXXX.szpm`

4. Install the szpm using the zammad package manager.

5. Repeat


### Create a new migration

Included is a helper script to create new migrations. You must have the python
`inflection` library installed.

* debian/ubuntu:  `apt install python3-inflection`
* pip: `pip install --user inflection`
* or create your own venv

To make a new migration simply run:
```
make new-migration
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/grabadora/zamamd-addon-grabadora/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).

🐻
