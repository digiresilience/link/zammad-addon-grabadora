.PHONY: build prep clean fmt new-migration

build: prep
	@./package.py
	@find dist/ -iname "*szpm"

prep:
	@mkdir -p dist

clean: prep
	@rm -rf dist/*

fmt:
	rufo --simple-exit src

new-migration:
	@./new-migration.py

test:
	@echo There are no tests
